FROM debian:buster-slim

ENV PWSH_VERSION 7.1.3
# Define ENVs for Localization/Globalization
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT false
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
# set a fixed location for the Module analysis cache
ENV PSModuleAnalysisCachePath /var/cache/microsoft/powershell/PSModuleAnalysisCache/ModuleAnalysisCache
ENV POWERSHELL_DISTRIBUTION_CHANNEL PSDocker-Debian-10

RUN set -eux; \
	# curl & xz-utils are helper tools to download and decompress file
	# less is required for help in powershell
	# locales is requied to setup the locale
	# openssh-client: PowerShell remoting over SSH dependencies
	# others is required for SSL
	requiedDeps="curl xz-utils \
		less locales openssh-client \
		ca-certificates gss-ntlmssp libicu63 libssl1.1 libc6 libgcc1 libgssapi-krb5-2 liblttng-ust0 libstdc++6 zlib1g"; \
	apt-get update && apt-get install -y --no-install-recommends $requiedDeps; \
	curl -sSL "https://github.com/PowerShell/PowerShell/releases/download/v${PWSH_VERSION}/powershell-${PWSH_VERSION}-linux-x64.tar.gz" -o pwsh.tar.gz; \
	mkdir -p /opt/microsoft/powershell; \
	tar zxf pwsh.tar.gz -C /opt/microsoft/powershell --strip-components=1; \
	rm -rf pwsh.tar.gz; \
	# Create the pwsh symbolic link that points to powershell
	ln -s /opt/microsoft/powershell/pwsh /usr/bin/pwsh; \
	# Clean cache and unnecessary deps
	buildDeps="curl xz-utils"; \
	apt-get purge -y --auto-remove $buildDeps; \
	rm -rf /var/cache/apt/*; \
	rm -rf /var/lib/apt/lists/*; \
	rm -rf /tmp/*; \
	rm -rf /var/tmp/*

# Use PowerShell as the default shell
# Use array to avoid Docker prepending /bin/sh -c
CMD [ "pwsh" ]

#!/bin/bash

set -e

GHOST_CONTENT="/ghost/content"
GHOST_INSTALL="/ghost"

# init some necessary files and folders
function init_dir() {
    DIRS=("apps" "data" "images" "logs" "settings" "themes")
    for DIR in ${DIRS[@]}; do
        if [ ! -e $GHOST_CONTENT/$DIR ]; then
            echo "Init $GHOST_CONTENT/$DIR directory..."
            mkdir -p $GHOST_CONTENT/$DIR

            if [ $DIR == "themes" ]; then
                cp -r $GHOST_INSTALL/current/content/themes/casper $GHOST_CONTENT/$DIR/
            fi
        fi
    done
}

# allow the container to be started with `--user`
if [[ "$*" == node*current/index.js* ]] && [ "$(id -u)" = '0' ]; then
	find "$GHOST_CONTENT" \! -user node -exec chown node '{}' +
	exec gosu node "$BASH_SOURCE" "$@"
fi

if [[ "$*" == node*current/index.js* ]]; then
    init_dir
fi

exec "$@"

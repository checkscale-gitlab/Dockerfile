# compile build stage
FROM debian:buster-slim AS builder

LABEL maintainer="Feng.YJ <i@huiyifyj.cn>"

ENV THRIFT_VERSION v0.13.0

RUN buildDeps="automake bison curl flex g++ make pkg-config libtool \
		libboost-dev libboost-filesystem-dev libboost-program-options-dev \
		libboost-system-dev libboost-test-dev libevent-dev libssl-dev"; \
	apt-get update && apt-get install -y --no-install-recommends $buildDeps; \
	curl -k -sSL "https://github.com/apache/thrift/archive/${THRIFT_VERSION}.tar.gz" -o thrift.tar.gz; \
	mkdir -p /usr/src/thrift; \
	tar zxf thrift.tar.gz -C /usr/src/thrift --strip-components=1; \
	rm -rf thrift.tar.gz; \
	cd /usr/src/thrift; \
	./bootstrap.sh; \
	./configure --disable-libs; \
	make; \
	make install; \
	# Convenient for `COPY` file in next stage
	mv `which thrift` /bin/thrift

# main stage
FROM debian:buster-slim

COPY --from=builder /bin/thrift /usr/local/bin/thrift

CMD [ "thrift" ]
